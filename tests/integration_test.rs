#[cfg(test)]
mod describe_the_rest_api {
    use app::app;
    use rocket::local::asynchronous::Client;

    async fn init() -> Client {
        return Client::tracked(app()).await.unwrap();
    }

    #[async_std::test]
    async fn it_can_get_a_url() {
        let _m = mockito::mock("GET", "/")
            .with_body(r#"{"url": "https://httpbin.org/get"}"#)
            .create();
        let client = init().await;
        let resp = client.get("/").dispatch().await;
        assert_eq!("https://httpbin.org/get", resp.into_string().await.unwrap());
    }
}
