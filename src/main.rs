mod config;

use crate::config::{load_config, setup_logger};
use app::app;
use log::info;

#[rocket::main]
async fn main() -> Result<(), rocket::Error> {
    let config = load_config().unwrap();
    setup_logger(&config.log_level).unwrap();
    info!("Running with: {:?}", config);
    let _guard = sentry::init((
        config.sentry_url,
        sentry::ClientOptions {
            release: sentry::release_name!(),
            ..Default::default()
        },
    ));
    let _rocket = app().launch().await?;
    Ok(())
}
