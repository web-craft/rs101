mod config;
mod domain;

#[macro_use]
extern crate rocket;

pub use config::load_config;
use rocket::{Build, Rocket};
use serde::{Deserialize, Serialize};
use std::io;

#[derive(Debug, Serialize, Deserialize)]
pub struct Resp {
    pub url: String,
}

#[get("/")]
async fn index() -> io::Result<String> {
    let config = load_config().unwrap();

    let url = match cfg!(debug_assertions) || config.e2e {
        true => "https://httpbin.org/get".to_string(),
        false => mockito::server_url(),
    };

    let res = reqwest::get(url)
        .await
        .unwrap()
        .json::<Resp>()
        .await
        .unwrap();

    Ok(res.url)
}

pub fn app() -> Rocket<Build> {
    let config = load_config().unwrap();
    let figment = rocket::Config::figment()
        .merge(("address", &config.host))
        .merge(("port", &config.port))
        .merge(("log_level", "off"));

    rocket::custom(figment).mount("/", routes![index])
}
